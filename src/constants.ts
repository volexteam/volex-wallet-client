export const BTC_DERIVATION_PATH = `m/44'/0'/0'/0`;
export const BTC_TESTNET_DERIVATION_PATH = `m/44'/1'/0'/0`;
export const ETH_DERIVATION_PATH = `m/44'/60'/0'/0`;
export const TRX_DERIVATION_PATH = `m/44'/195'/0'/0`;
