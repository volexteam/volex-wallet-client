import bitcore, { Transaction, Unit } from "bitcore-lib";
import BIP32Factory from "bip32";
import * as ecc from "tiny-secp256k1";
// import { BIP32Interface } from "bip32";
import * as bip39 from "bip39";
import EthWallet from "ethereumjs-wallet";
import axios from "axios";
import BigNumber from "bignumber.js";
import Web3 from "web3";
import { AbiItem } from "web3-utils";

// eslint-disable-next-line @typescript-eslint/no-var-requires
const TronWeb = require("tronweb");

axios.defaults.baseURL = "http://127.0.0.1:3000";
axios.defaults.headers.common["x-api-key"] = process.env.VOLEX_API_KEY || "";

import {
  BTC_DERIVATION_PATH,
  BTC_TESTNET_DERIVATION_PATH,
  ETH_DERIVATION_PATH,
  TRX_DERIVATION_PATH,
} from "./constants";

const bip32 = BIP32Factory(ecc);

export const getChains = async ({
  limit,
  offset,
}: {
  limit: number;
  offset: number;
}): Promise<{ symbol: string; title: string }[]> => {
  const res = await axios("/chains", {
    params: { limit, offset },
  });
  return res.data.data.data;
};

export const generateMnemonic = bip39.generateMnemonic;

export const generateKeyPairFromMnemonic = ({
  testnet,
  chain,
  mnemonic,
  index,
}: {
  testnet: boolean;
  chain: string;
  mnemonic: string;
  index: number;
}) => {
  switch (chain) {
    case "BTC": {
      const privateKey =
        bip32
          .fromSeed(
            bip39.mnemonicToSeedSync(mnemonic),
            testnet
              ? {
                  messagePrefix: "\x18Bitcoin Signed Message:\n",
                  bech32: "tb",
                  bip32: {
                    public: 0x043587cf,
                    private: 0x04358394,
                  },
                  pubKeyHash: 0x6f,
                  scriptHash: 0xc4,
                  wif: 0xef,
                }
              : undefined
          )
          .derivePath(
            testnet ? BTC_TESTNET_DERIVATION_PATH : BTC_DERIVATION_PATH
          )
          .derive(index)
          .toWIF() || "";
      return {
        privateKey,
        address:
          new bitcore.PrivateKey(privateKey).toAddress().toString() || "",
      };
    }
    case "ETH": {
      const privateKey =
        bip32
          .fromSeed(bip39.mnemonicToSeedSync(mnemonic))
          .derivePath(ETH_DERIVATION_PATH)
          .derive(index)
          .privateKey?.toString("hex") || "";

      return {
        privateKey: "0x" + privateKey,
        address:
          "0x" +
            EthWallet.fromPrivateKey(Buffer.from(privateKey || "", "hex"))
              .getAddress()
              .toString("hex") || "",
      };
    }
    case "TRX": {
      const privateKey =
        bip32
          .fromSeed(bip39.mnemonicToSeedSync(mnemonic))
          .derivePath(TRX_DERIVATION_PATH)
          .derive(index)
          .privateKey?.toString("hex") || "";

      return {
        privateKey,
        address: TronWeb.address.fromPrivateKey(privateKey) as string,
      };
    }
    default:
      throw Error("invalid chain");
  }
};

export const getDepositAddress = async ({
  chain,
  address,
  limit,
  offset,
}: {
  chain: string;
  address: string;
  limit: number;
  offset: number;
}): Promise<{ chain: string; address: string }[]> => {
  const res = await axios("/addresses", {
    params: { chain, address, limit, offset },
  });
  return res.data.data.data;
};

export const setDepositAddress = async ({
  chain,
  address,
}: {
  chain: string;
  address: string;
}) => {
  await axios.post("/addresses", {
    chain,
    address,
  });
};

export const unsetDepositAddress = async ({
  chain,
  address,
}: {
  chain: string;
  address: string;
}) => {
  await axios.delete("/addresses", {
    data: {
      chain,
      address,
    },
  });
};

export const sendTransaction = async ({
  testnet,
  chain,
  from,
  to,
  fromPrivateKey,
  amount,
  contractAddress,
  fee,
  gasLimit,
  gasPrice,
  nonce,
  changeAddress,
}: {
  testnet: boolean;
  chain: string;
  from?: string | string[];
  to: string | { address: string; amount: string }[];
  fromPrivateKey: string;
  amount: string;
  contractAddress?: string;
  fee?: string;
  gasLimit?: string;
  gasPrice?: string;
  nonce?: number;
  changeAddress?: string;
}) => {
  switch (chain) {
    case "TRX": {
      const tronWeb = new TronWeb({
        fullHost: "https://api" + (testnet ? ".shasta" : "") + ".trongrid.io",
        privateKey: fromPrivateKey,
      });
      let transaction;
      if (contractAddress) {
        tronWeb.setAddress(contractAddress);
        const contractInstance = await tronWeb.contract().at(contractAddress);
        const decimals = await contractInstance.decimals().call();
        transaction = (
          await tronWeb.transactionBuilder.triggerSmartContract(
            tronWeb.address.toHex(contractAddress),
            "transfer(address,uint256)",
            {
              feeLimit: tronWeb.toSun(fee),
              from: tronWeb.address.fromHex(
                tronWeb.address.fromPrivateKey(fromPrivateKey)
              ),
            },
            [
              { type: "address", value: tronWeb.address.toHex(to) },
              {
                type: "uint256",
                value: `0x${new BigNumber(amount)
                  .multipliedBy(new BigNumber(10).pow(decimals))
                  .toString(16)}`,
              },
            ],
            tronWeb.address.fromHex(
              tronWeb.address.fromPrivateKey(fromPrivateKey)
            )
          )
        ).transaction;
      } else
        transaction = await tronWeb.transactionBuilder.sendTrx(
          to,
          tronWeb.toSun(amount)
        );
      const signedTransaction = await tronWeb.trx.sign(
        transaction,
        fromPrivateKey
      );
      const res = await axios.post("/transactions", {
        chain,
        txData: signedTransaction,
      });
      return res.data.data;
    }
    case "ETH": {
      const web3 = new Web3(
        testnet
          ? "https://ropsten.infura.io/v3/f5e2d42b5d184cabb5c9cb0dfce645cc"
          : "https://api.mycryptoapi.com/eth"
      );
      let data;
      if (contractAddress) {
        const abi: AbiItem[] = [
          {
            constant: true,
            inputs: [],
            name: "decimals",
            outputs: [{ name: "", type: "uint256" }],
            payable: false,
            stateMutability: "view",
            type: "function",
          },
          {
            constant: false,
            inputs: [
              { name: "_to", type: "address" },
              { name: "_value", type: "uint256" },
            ],
            name: "transfer",
            outputs: [],
            payable: false,
            stateMutability: "nonpayable",
            type: "function",
          },
        ];
        const contract = new web3.eth.Contract(abi, contractAddress);
        const decimals = await contract.methods.decimals().call();
        data = contract.methods
          .transfer(
            to,
            `0x${new BigNumber(amount)
              .multipliedBy(new BigNumber(10).pow(decimals))
              .toString(16)}`
          )
          .encodeABI();
      }
      const tx = await web3.eth.accounts.signTransaction(
        {
          nonce,
          from: from + "",
          to: contractAddress ?? to + "",
          value: web3.utils.toWei(contractAddress ? "0" : amount, "ether"),
          gasPrice: gasPrice
            ? web3.utils.toWei(gasPrice || "", "gwei")
            : undefined,
          gas: gasLimit,
          data,
        },
        fromPrivateKey
      );
      const res = await axios.post("/transactions", {
        chain,
        txData: tx?.rawTransaction,
      });
      return res.data.data;
    }
    case "BTC": {
      const tx = new Transaction();
      const utxos = [];
      const privKeystoSign = [];
      for (const privateKey of from || []) {
        const address =
          new bitcore.PrivateKey(privateKey).toAddress().toString() || "";
        const result = await axios(
          `https://api.bitcore.io/api/BTC/testnet/address/${address}?unspent=true&limit=1000`
        );
        for (const item of result.data) {
          utxos.push({
            txId: item.mintTxid,
            outputIndex: item.mintIndex,
            script: item.script,
            satoshis: item.value,
          });
          // look through database and find derivation path
          privKeystoSign.push(privateKey);
        }
      }
      tx.from(utxos as Transaction.UnspentOutput[]);
      for (const { address, amount } of to as {
        address: string;
        amount: string;
      }[])
        tx.to(address, Unit.fromBTC(+amount).toSatoshis());
      tx.change(changeAddress || "");
      tx.fee(+(fee || 0.00003) * 100000000);
      for (const priv of privKeystoSign) tx.sign(priv);

      const res = await axios.post("/transactions", {
        chain,
        txData: tx.serialize(),
      });
      return res.data.data;
    }
  }
};
